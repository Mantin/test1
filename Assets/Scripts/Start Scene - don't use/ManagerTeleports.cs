﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ManagerTeleports : MonoBehaviour {

    void OnTriggerEnter(Collider sender)
    {
        if (sender.tag == "Player")
        {
            Cursor.visible = true;
            SceneScript.SceneName = "SceneMenu";
            SceneManager.LoadScene("SceneLoadingFin");
        }
    }

}
