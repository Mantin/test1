﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateMap : MonoBehaviour {

    public GameObject[] objectWay;

    public float distance; // Дистанция до объекта.
    float Angle = 360f; 
    int countObjects = 5; // Количество направлений.

    void Start()
    {
        createWays();
    }

    void createWays()
    {
        for (int i = 0; i < countObjects; i++)
        {
            float angleObject = (Angle / countObjects) * i;
            float angleInRad = angleObject * Mathf.Deg2Rad;
            Vector3 point = new Vector3(
                distance * Mathf.Sin(angleInRad),
                0f,
                distance * Mathf.Cos(angleInRad)
            );
            objectWay[i].transform.position = point;
            objectWay[i].transform.rotation = Quaternion.Euler(0f, angleObject, 0f);
        }
    }

}
