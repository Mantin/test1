﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchMaterial : MonoBehaviour {

    public Material[] materialRoof;
    public Material[] materialFloor;
    public Material[] materialWall;

    // Смена материала объекту.
    public void changeMaterial(int index, GameObject gameObject)
    {
        int indexMaterial =  index % 2 == 0 ? 0 : 1;

        foreach (Transform gameObj in gameObject.transform)
            setMaterial(gameObj, indexMaterial);
    }

    // Определяем объекту соответствующий материал.
    void setMaterial(Transform gameObject, int indexMaterial)
    {
        Renderer render = gameObject.GetComponent<Renderer>();
        switch (gameObject.name)
        {
            case "Top":
                render.material = materialRoof[indexMaterial];
                break;
            case "Bottom":
                render.material = materialFloor[indexMaterial];
                break;
            case "Left":
                render.material = materialWall[indexMaterial];
                break;
            case "Right":
                render.material = materialWall[indexMaterial];
                break;
            default:
                break;
        }
        
    }

}
