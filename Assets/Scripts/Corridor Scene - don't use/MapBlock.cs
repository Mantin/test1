﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBlock : MonoBehaviour {

    public GameObject Block;
    public float stepPosition = 10f; // Длина блока.
    public int lengthCorridor = 11; // Количество блоков корридора.

    SwitchMaterial switchMaterial;

    Vector3 turnForward = new Vector3(0f, 0f, 0f);
    Vector3 turnBack = new Vector3(0f, 180f, 0f);
    Vector3 turnRight = new Vector3(0f, 90f, 0f);
    Vector3 turnLeft = new Vector3(0f, -90f, 0f);

    void Awake()
    {
        switchMaterial = GetComponent<SwitchMaterial>();
    }

    #region [Create a new path]

    public void createBlocks(int direction, List<GameObject> listBlocks)
    {
        for (int i = 0; i < lengthCorridor; i++)
        {
            listBlocks.Add(createBlock(i, direction));
        }
    }

    public GameObject createBlock(int numberBlock, int direction)
    {
        Quaternion rotation = Quaternion.Euler(turnForward);
        Vector3 position = new Vector3(0f, 0f, stepPosition * (numberBlock + 1) * direction);

        return Instantiate(Block, position, rotation);
    } 

    #endregion

}
