﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerMap : MonoBehaviour {

    // Direction
    const int Forward = 1;
    const int Back = -1;

    List<GameObject> listForwardBlocks; // Список объектов сцены (Block_Forward).
    List<GameObject> listBackBlocks; // Список объектов сцены (Block_Back).
    GameObject player;
    MapBlock block;

    void Awake()
    {
        listForwardBlocks = new List<GameObject>();
        listBackBlocks = new List<GameObject>();
        player = GameObject.Find("Camera");
        block = GetComponent<MapBlock>();
    }

    void Start()
    {
        createStartCorridor(); // Создание первоначальной сцены.
    }

    void FixedUpdate()
    {
        // Воссоздание корридора.
        fillMap(checkPlayerPosition());
    }

    #region [Control Map]

    void createStartCorridor()
    {
        block.createBlocks(Forward, listForwardBlocks);
        block.createBlocks(Back, listBackBlocks);
    }

    int checkPlayerPosition()
    {
        return (int)player.transform.position.z / (int)block.stepPosition;
    }

    void fillMap(int thisBlock)
    {
        int direction = thisBlock > 0 ? Forward : Back;

        switch(direction)
        {
            case Forward:
                checkFillMap(thisBlock, listForwardBlocks, direction);
                break;
            case Back:
                checkFillMap(thisBlock, listBackBlocks, direction);
                break;
            default:
                break;
        }
    }

    void checkFillMap(int thisBlock, List<GameObject> list, int direction)
    {
        var thisCountCorridor = list.Count - Mathf.Abs(thisBlock);
        if (thisCountCorridor < block.lengthCorridor)
            list.Add(block.createBlock(list.Count, direction));
    }

    #endregion
}
