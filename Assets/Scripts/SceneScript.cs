﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneScript : MonoBehaviour {

	static public string SceneName;
	WarpSpeed ws;
	public float engageTime;
	public float disengageTime;
	public float endTime;
	float time = 0.0f;
	float substraction;
	ParticleSystem.MainModule wsMain;

	// Use this for initialization
	void Start () {
		ws = GetComponent<WarpSpeed>();
		ws.Speed = 5.0f;
		substraction = 1 / (endTime - disengageTime); 
		wsMain = ws.GetComponent<ParticleSystem>().main;

	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(time);
		time += Time.deltaTime;
		if (time > engageTime)
		{
			ws.Engage();
		}
		if (time > disengageTime)
		{
			ws.Disengage();
			wsMain.simulationSpeed -= substraction * Time.deltaTime;
		}
		if (time >= endTime)
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene(SceneName);
		}

	}
}
