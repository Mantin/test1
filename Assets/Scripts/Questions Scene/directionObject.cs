﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class directionObject : MonoBehaviour {

    public GameObject player;
    public GameObject panel;
    public GameObject managerQuestions;

    // Переход к экрану (отключение скриптов управления).
    void OnTriggerEnter(Collider sender)
    {
        if (sender.tag == "Player")
        {
         // player.GetComponent<playerMovement>().enabled = false;
            player.transform.position = transform.position;
            Vector3 relativePos = panel.transform.position - player.transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            player.transform.rotation = rotation;
            managerQuestions.GetComponent<ManagerQuestions>().enabled = true;
        }
    }
}
