﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour {

    public float speed = 1f;
    public float sensitivity = 1f;
    public float timeSwitchWay = 2.5f;
    public float lerpStep = .2f;
    public float randomRangeRotation = .1f;
    public float randomRangePosition = .5f;

    Vector3 rotation;
    float currentTime = 0f;
    float randomValueRotationX = 0f;
    float randomValueRotationY = 0f;
    float randomValuePositionY = 0f;

    void Start()
    {
        directionRangeRotation();
    }

    void FixedUpdate()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= timeSwitchWay)
        {
            directionRangeRotation();
            currentTime = 0f;
        }
        Turning(randomValueRotationX, randomValueRotationY, 5f);
        //Move();
    }

    void Move()
    {
        float posY = Mathf.Clamp(transform.position.y, 2f, 4f);
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, posY + randomValuePositionY, transform.position.z), speed);
    }

    void Turning(float x, float y, float maxAngleRotationXZ)
    {
        rotation.Set(x, y, 0f);
        Vector3 currentRotation = transform.eulerAngles + (rotation * sensitivity);
        float currentX = Clamp(currentRotation.x, -maxAngleRotationXZ, maxAngleRotationXZ);
        Debug.Log("OUT: " + currentRotation);
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(currentX, currentRotation.y, currentRotation.z), lerpStep);
    }

    void directionRangeRotation()
    {
        float x = Random.Range(-randomRangeRotation, randomRangeRotation);
        x = x == 0f ? 0f : x < 0f ? -randomRangeRotation : randomRangeRotation;
        float yRotation = Random.Range(-randomRangeRotation, randomRangeRotation);
        yRotation = yRotation == 0f ? 0f : yRotation < 0f ? -randomRangeRotation : randomRangeRotation;
        float yPosition = Random.Range(-randomRangePosition, randomRangePosition);
        yPosition = yPosition == 0f ? 0f : yPosition < 0f ? -randomRangePosition : randomRangePosition;

        randomValueRotationX = x;
        randomValueRotationY = yRotation;
        randomValuePositionY = yPosition;
    }

    float Clamp(float currentValue, float minValue, float maxValue)
    {     
        return currentValue < minValue ? minValue : currentValue > maxValue ? maxValue : currentValue;
    }
}
