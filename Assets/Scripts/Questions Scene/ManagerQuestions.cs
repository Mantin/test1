﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ManagerQuestions : MonoBehaviour {

    public static int[] matrixActivites;
    public GameObject startQuestions;
    public GameObject camera;
    public GameObject wall_right;
    public GameObject wall_left;

    public GameObject[] objectsActivites;
    public Material blue;
    public Material white;

    public Text question_one;
    public Text question_two;

    int current = 0;
    int next = 1;
    int[,] matrix;
    int[] matrixNeeds;
    int[,] matrixOut;

    string[] questions = new string[]
    {
        "...чтобы учеба (работа) доставляла мне удовольствие, и изменения в ней не создавали дополнительных хлопот;",
        "...встречаться и общаться с приятными мне людьми;",
        "...предчувствовать надвигающиеся события и получать удовольствие от самостоятельного познания жизни;",
        "...вести активный образ жизни и быть удовлетворенным своей готовностью к будущему;",
        "...управлять хотя бы кем-нибудь;",
        "...достигать поставленных целей в своей учебе (работе);",
        "...общаться с новыми интересными людьми;",
        "...постоянно расширять свой кругозор;",
        "...быть готовым принимать решения, имеющие последствия для окружающих меня людей;",
        "...занять заметное положение в обществе;",
        "...чтобы мои способности ярко проявлялись в учебе (работе);",
        "...быть тактичным в общении;",
        "...познать смысл жизни;",
        "...знать свои возможности в предстоящих переменах;",
        "...достичь сотрудничества с управляемым мною коллективом."
    };

    string[] randomizeQuestions;

    void Awake()
    {
        camera.GetComponent<playerMovement>().enabled = false;
    }

    void Start()
    {
        matrix = new int[questions.Length, questions.Length];
        createStaticMassiv(questions.Length);
        checkNextQuestions();
        showQuestions();
    }

    void createStaticMassiv(int max)
    {
        matrixOut = new int[max, max];
        for (int i = 0; i < max; i++)
        {
            for (int j = 0; j < max; j++)
            {
                matrixOut[i,j] = 0;
                if (i == j)
                    matrixOut[i, j] = -1;
            }
        }
    }

    public void nextScene()
    {
        SceneManager.LoadScene("SceneLoading");
    }

    public void addPointCurrentQuestion()
    {
        matrix[current, next]++;
        checkNextQuestions();
        showQuestions();
    }

    public void addPointNoCurrentQuestion()
    {
        matrix[next, current]++;
        checkNextQuestions();
        showQuestions();
    }

    bool checkEmpty(int max)
    {
        for (int i = 0; i < max; i++)
        {
            for (int j = 0; j < max; j++)
            {
                if (matrixOut[i, j] == 0)
                    return false;
            }
        }
        return true;

    }

    // Переходы между вопросами
    void checkNextQuestions()
    {
        int a, b;
        do
        {
            a = (int)Random.Range(0, 15);
            b = (int)Random.Range(0, 15);
        }
        while (matrixOut[a, b] < 0 || a == b );

        matrixOut[a, b]--;
        matrixOut[b, a]--;
        current = a;
        next = b;
         
        if (checkEmpty(questions.Length))
        {
            TPManager asdasdas = GameObject.Find("TP Manager").GetComponent<TPManager>();
            wall_left.SetActive(false);
            wall_right.SetActive(false);
            startQuestions.SetActive(false);
            camera.GetComponent<playerMovement>().enabled = true;
            directionPoint();
            directionPointToActivites();
            fillDiagramm();
            asdasdas.Proceed();
        }
    }

    // Подсчёт очков ответа.
    void directionPoint()
    {
        int k;
        matrixNeeds = new int[questions.Length];
        for (int i = 0; i < questions.Length; i++)
        {
            k = 0;
            for (int j = 0; j < questions.Length; j++)
            {
                k += matrix[i, j];
            }
            matrixNeeds[i] = k;
        }
    }

    int maxIndex = 0;
    int maxMatrixActivites = 0;
    // Подсчёт очков к видам деятельности
    void  directionPointToActivites()
    {
        matrixActivites = new int[5];
        // 0 - Труд, 1 - Общение, 2 - Познание, 3 - Рекреация, 4 - Управление.
        matrixActivites[0] = (matrixNeeds[0] + matrixNeeds[5] + matrixNeeds[10]) / 3;
        matrixActivites[1] = (matrixNeeds[1] + matrixNeeds[6] + matrixNeeds[11]) / 3;
        matrixActivites[2] = (matrixNeeds[2] + matrixNeeds[7] + matrixNeeds[12]) / 3;
        matrixActivites[3] = (matrixNeeds[3] + matrixNeeds[8] + matrixNeeds[13]) / 3;
        matrixActivites[4] = (matrixNeeds[4] + matrixNeeds[9] + matrixNeeds[14]) / 3;
        for (int i = 0; i < matrixActivites.Length; i++)
        {
            if (matrixActivites[i] > maxMatrixActivites)
            {
                maxIndex = i;
                maxMatrixActivites = matrixActivites[i];
            }
        }
    }

    // Отображение вопросов.
    void showQuestions()
    {
        question_one.text = questions[current];
        question_two.text = questions[next];
    }

    // Заполнение диаграммы
    void fillDiagramm()
    {
        for (int i = 0; i < objectsActivites.Length; i++)
        {
            Transform[] gameObj = new Transform[objectsActivites[i].transform.childCount];
            for (int n = 0; n < objectsActivites[i].transform.childCount; n++)
            {
                gameObj[n] = objectsActivites[i].transform.GetChild(n);
            }
            
            int k = (int)((matrixActivites[i] > 12 ? 12 : matrixActivites[i]) / 4);
            for (int j = 0; j < k; j++)
            {
                if (i != maxIndex)
                gameObj[j].gameObject.GetComponent<Renderer>().material = blue;
                else
                    gameObj[j].gameObject.GetComponent<Renderer>().material = white;

            }
        }
    }

}