﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerMenu : MonoBehaviour {

    public void clickPlay()
    {
        SceneScript.SceneName = "SceneStart";
        SceneManager.LoadScene("SceneLoadingFin"); 
    }

    //public void clickGallary()
    //{
    //    SceneManager.LoadScene("SceneGallary");
    //}

    public void clickExit()
    {
        Application.Quit();
    }

}
