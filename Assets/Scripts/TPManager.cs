﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPManager : MonoBehaviour {

	public GameObject Masterstvo;
	public GameObject Obshchenie;
	public GameObject Otdih;
	public GameObject Poznanie;
	public GameObject Upravlenie;

	int[] results;

	GameObject[] getAllChilds(GameObject parent){
		GameObject[] res = new GameObject[parent.transform.childCount];
		for (int i = 0; i < parent.transform.childCount; i++)
		{
			res[i] = parent.transform.GetChild(i).gameObject;
		}
		return res;
	}


	public void Proceed () {
		results = ManagerQuestions.matrixActivites;
		List<GameObject[]> AllElems = new List<GameObject[]>();
		GameObject[] MasterstvoChilds = getAllChilds(Masterstvo);
		AllElems.Add(MasterstvoChilds);
		GameObject[] ObshchenieChilds = getAllChilds(Obshchenie);
		AllElems.Add(ObshchenieChilds);
		GameObject[] OtdihChilds = getAllChilds(Otdih);
		AllElems.Add(OtdihChilds);
		GameObject[] PoznanieChilds = getAllChilds(Poznanie);
		AllElems.Add(PoznanieChilds);
		GameObject[] UpravlenieChilds = getAllChilds(Upravlenie);
		AllElems.Add(UpravlenieChilds);

		for (int i = 0; i < AllElems.Count; i++)
		{
            for (int j = 0; j < results[i] / 4; j++)
            {
                AllElems[i][j].SetActive(true);
            }
            AllElems[i][AllElems[i].Length - 1].SetActive(true);
		}
	}

	public void toMM(){
		UnityEngine.SceneManagement.SceneManager.LoadScene("SceneMenu");
	}

}
