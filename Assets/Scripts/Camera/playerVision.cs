﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class playerVision : MonoBehaviour {

    public float range = 5f;
    public int time = 3;
    float speed;
    float currentAmount;
    DateTime startTime;

    bool loadingNewScene;

    public GameObject radialProgressBar;
    public Image loadingBar;
    public Text textIndicator;

    Ray shootRay;
    RaycastHit shootHit;
    int shootableMask;

    void Awake()
    {
        shootableMask = LayerMask.GetMask("picture");
        radialProgressBar.SetActive(false);
        speed = 100f / time;
        loadingNewScene = false;
    }

    void Update()
    {
        checkLooking();
    }

    void checkLooking()
    {
        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask) && !loadingNewScene)
        {
            toNewScene();
        }
        else
        {
            radialProgressBar.SetActive(false);
        }
    }

    void toNewScene()
    {
        if (!radialProgressBar.active)
        {
            currentAmount = 0f;
            radialProgressBar.SetActive(true);
            startTime = DateTime.Now;
        }
        loadingBar.fillAmount = currentAmount / 100f;
        if (currentAmount < 100f)
        {
            currentAmount += speed * Time.deltaTime;
            TimeSpan ts = DateTime.Now - startTime;
            textIndicator.text = (time - ts.Seconds).ToString();
        }
        else
            loadingNewScene = true;
    }
}
