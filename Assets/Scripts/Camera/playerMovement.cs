﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour {

    public float speed = 6f;
    public float sensitivity = 2f;
    public float v;
    public float h;
    public float x;
    public float y;

    Vector3 movement;
    Vector3 rotation;
    Rigidbody playerRigidbody;

    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        Cursor.visible = false;
    } 

    void FixedUpdate()
    {
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");

        x = Input.GetAxis("Mouse X");
        y = Input.GetAxis("Mouse Y");

        Move(h, v);
        Turning();
    }

    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = transform.TransformDirection(movement.normalized) * speed * Time.deltaTime;
        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        rotation.Set(-y, x, 0f);
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, transform.eulerAngles+(rotation * sensitivity), .7f);
    }

}
//var angle = 0f;
//angle = sensitivity * ((rotation.x - (Screen.width / 2)) / Screen.width);
////transform.RotateAround(transform.eulerAngles, transform.up, angle);
//angle = sensitivity * ((rotation.y - (Screen.height / 2)) / Screen.height);
////transform.RotateAround(transform.eulerAngles, transform.right, -angle);