﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatingWay : MonoBehaviour {

    public float speed = 10f;
    public float sensitivity = 1f;
    public float timeSwitchWay = 2.5f;
    public float lerpStep = .2f;
    public float randomRange = .1f;

    Vector3 rotation;
    float currentTime = 0f;
    float randomValueRotationX = 0f;
    float randomValueRotationY = 0f;

    void Start()
    {
        directionRangeRotation();
    }

    void FixedUpdate()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= timeSwitchWay)
        {
            directionRangeRotation();
            currentTime = 0f;
        }
        Turning(randomValueRotationX, randomValueRotationY);
        transform.position += new Vector3(0f, 0f, 1f) * speed * Time.deltaTime;
    }

    void Turning(float x, float y)
    {
        rotation.Set(x, y, 0f);
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, transform.eulerAngles + (rotation * sensitivity), lerpStep);
    }

    void directionRangeRotation()
    {
        float x = Random.Range(-randomRange, randomRange);
        x = x == 0f ? 0f : x < 0f ? -randomRange : randomRange;
        float y = Random.Range(-randomRange, randomRange);
        y = y == 0f ? 0f : y < 0f ? -randomRange : randomRange;

        randomValueRotationX = x;
        randomValueRotationY = y;
    }

}
